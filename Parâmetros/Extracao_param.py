# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 22:29:36 2021

@author: embedded05
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import imp

lumapi = imp.load_source("Lumapi","C:\\Program Files\\Lumerical\\v211\\api\\python\\Lumapi.py")

ic1 = lumapi.INTERCONNECT("testeH.icp")

p1 = ic1.getresult("ONA_1","input 1/mode 1/gain")  #Ch1 - Qp
p2 = ic1.getresult("ONA_1","input 2/mode 1/gain")  #Ch2 - Qn
p3 = ic1.getresult("ONA_1","input 3/mode 1/gain")  #Ch3 - In
p4 = ic1.getresult("ONA_1","input 4/mode 1/gain")  #Ch4 - Ip

lbd = p1['wavelength']
g1 = p1['mode 1 gain (dB)']
g2 = p2['mode 1 gain (dB)']
g3 = p3['mode 1 gain (dB)']
g4 = p4['mode 1 gain (dB)']

#Imbalance

Inb_Q = g1 - g2  #Inbalance Q = 10log(P_Qp/P_Qn)
Inb_I = g4 - g3  #Inbalance I = 10log(P_Ip/P_In)

fig,axs = plt.subplots(2,1,figsize =(10,6))
fig.text(0.06, 0.5, 'Imbalance (dB)',fontsize = 12,fontstyle = 'italic', ha='center', va='center', rotation='vertical')
fig.text(0.5, 0.04, r'$\lambda (nm)$',fontsize = 12,fontstyle = 'italic', ha='center', va='center')

axs[0].plot(1e9*lbd,Inb_I,linewidth = 1.4,color = 'green',label = 'Canal I')
axs[0].set_xlim(1530,1565)
axs[0].set_ylim(0,0.5)
axs[0].grid()
axs[0].legend()

axs[1].plot(1e9*lbd,Inb_Q,linewidth = 1.4,color = 'blue',label = 'Canal Q')
axs[1].set_xlim(1530,1565)
axs[1].set_ylim(0,0.5)
axs[1].grid()
axs[1].legend()

# CMRR

CMRR_I = - 20*np.log10( np.abs( ( 10**(g4/10) - 10**(g3/10) ) / ( 10**(g4/10) + 10**(g3/10) ) ) )
CMRR_Q = - 20*np.log10( np.abs( ( 10**(g1/10) - 10**(g2/10) ) / ( 10**(g1/10) + 10**(g2/10) ) ) )

fig,axs = plt.subplots(2,1,figsize =(10,6))
fig.text(0.07, 0.5, 'CMRR (dB)',fontsize = 12,fontstyle = 'italic', ha='center', va='center', rotation='vertical')
fig.text(0.5, 0.04, r'$\lambda (nm)$',fontsize = 12,fontstyle = 'italic', ha='center', va='center')

axs[0].plot(1e9*lbd,CMRR_I,linewidth = 1.4,color = 'green',label = 'Canal I')
axs[0].set_xlim(1530,1565)
axs[0].set_ylim(20,60)
axs[0].grid()
axs[0].legend()

axs[1].plot(1e9*lbd,CMRR_Q,linewidth = 1.4,color = 'blue',label = 'Canal Q')
axs[1].set_xlim(1530,1565)
axs[1].set_ylim(20,60)
axs[1].grid()
axs[1].legend()


#Insertion Loss

P1 = ic1.getresult("ONA_2","input 1/mode 1/gain")  #Ch1 - Qp
P2 = ic1.getresult("ONA_2","input 2/mode 1/gain")  #Ch2 - Qn
P3 = ic1.getresult("ONA_2","input 3/mode 1/gain")  #Ch3 - In
P4 = ic1.getresult("ONA_2","input 4/mode 1/gain")  #Ch4 - Ip

lbd = P1['wavelength']
G1 = P1['mode 1 gain (dB)']
G2 = P2['mode 1 gain (dB)']
G3 = P3['mode 1 gain (dB)']
G4 = P4['mode 1 gain (dB)']

insLoss_LO = -10*np.log(10**(g1/10) + 10**(g2/10) + 10**(g3/10) + 10**(g4/10))
insLoss_SIG = -10*np.log(10**(G1/10) + 10**(G2/10) + 10**(G3/10) + 10**(G4/10))

fig,axs = plt.subplots(2,1,figsize =(10,6))
fig.text(0.06, 0.5, 'Total insertion loss (dB)',fontsize = 12,fontstyle = 'italic', ha='center', va='center', rotation='vertical')
fig.text(0.5, 0.04, r'$\lambda (nm)$',fontsize = 12,fontstyle = 'italic', ha='center', va='center')

axs[0].plot(1e9*lbd,insLoss_LO,linewidth = 1.4,color = 'green',label = 'Input from LO port')
axs[0].set_xlim(1530,1565)
axs[0].set_ylim(0,1)
axs[0].grid()
axs[0].legend()

axs[1].plot(1e9*lbd,insLoss_SIG,linewidth = 1.4,color = 'blue',label = 'Input from signal port')
axs[1].set_xlim(1530,1565)
axs[1].set_ylim(0,1)
axs[1].grid()
axs[1].legend()

#Phase error
ic2 = lumapi.INTERCONNECT("teste_angulo.icp")   #Dados com o laser na entrada do sinal

a1 = ic2.getresult("ONA_1","input 1/mode 1/angle")
a2 = ic2.getresult("ONA_1","input 2/mode 1/angle") 
a3 = ic2.getresult("ONA_1","input 3/mode 1/angle") 
a4 = ic2.getresult("ONA_1","input 4/mode 1/angle")  
a5 = ic2.getresult("ONA_1","input 5/mode 1/angle")
a6 = ic2.getresult("ONA_1","input 6/mode 1/angle")

a7 = ic2.getresult("ONA_1","input 5/mode 1/gain")
a8 = ic2.getresult("ONA_1","input 6/mode 1/gain")

phi1 = a1['TE angle (deg)']  # Fase do bend 1
phi2 = a2['TE angle (deg)']  # Fase do bend 2
phi3 = a3['TE angle (deg)']  # Fase do bend 3
phi4 = a4['TE angle (deg)']  # Fase do bend 4

g3 = a7['mode 1 gain (dB)']  # Ganho na porta 3
g4 = a8['mode 1 gain (dB)']  # Ganho na porta 4

teta = a5["mode 1 angle (deg)"] - a6["mode 1 angle (deg)"]  # Diferença de fase nas saídas do MMI

angle = (phi2 - phi1) + (phi4 - phi3) + teta
phase_error = 90 - (angle - 360*(angle//360))

plt.figure(4)
plt.plot(1e9*lbd,phase_error,linewidth = 1.4,color = 'blue')
plt.xlabel(r'$\lambda (nm)$',fontsize = 12,fontstyle = 'italic')
plt.ylabel('Phase error (° )',fontsize = 12,fontstyle = 'italic')
plt.xlim(1530,1565)
plt.grid()

plt.figure(5)
plt.plot(1e9*lbd,teta,linewidth = 1.4,color = 'red')
plt.xlabel(r'$\lambda (nm)$',fontsize = 12,fontstyle = 'italic')
plt.ylabel('Diferença de fase (° )',fontsize = 12,fontstyle = 'italic')
plt.ylim(88.8,90)
plt.xlim(1530,1565)
plt.grid()

plt.figure(6)
plt.plot(1e9*lbd,g3,linewidth = 1.4,color = 'blue',label = 'gain 3')
plt.plot(1e9*lbd,g4,linewidth = 1.4,color = 'green',label = 'gain 4')
plt.xlabel(r'$\lambda (nm)$',fontsize = 12,fontstyle = 'italic')
plt.ylabel('Gain (dB)',fontsize = 12,fontstyle = 'italic')
plt.xlim(1530,1565)
plt.legend()
plt.grid()