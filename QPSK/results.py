import numpy as np
import matplotlib.pyplot as plt
import os
import imp

from IPython import get_ipython

#os.add_dll_directory("C://Program Files//Lumerical//v212//api//python")
#lumapi = imp.load_source("lumapi","C://Program Files//Lumerical//v212//api//python//lumapi.py")

lumapi = imp.load_source("Lumapi","C:\\Program Files\\Lumerical\\v211\\api\\python\\Lumapi.py")

INTER = lumapi.INTERCONNECT("QPSK.icp")

# Signal I

sigI = INTER.getresult("OSC_1","signal")
sigI_time = sigI['time']
sigI_amp  = sigI['amplitude (a.u.)']

plt.figure(1)
plt.plot(sigI_time[500:len(sigI_time)],sigI_amp[500:len(sigI_time)], linewidth=1.4, label = 'Signal-I')
plt.xlabel('Time (s)',fontsize=12,fontstyle = 'italic');
plt.ylabel('Amplitude (a.u.)',fontsize=12,fontstyle = 'italic');
plt.legend(loc = 'upper right',facecolor='white', framealpha=1);
plt.grid()
plt.show()

# Signal Q

sigQ = INTER.getresult("OSC_2","signal")
sigQ_time = sigQ['time']
sigQ_amp  = sigQ['amplitude (a.u.)']

plt.figure(2)
plt.plot(sigQ_time[500:len(sigQ_time)],sigQ_amp[500:len(sigQ_time)], linewidth=1.4, label='Signal-Q')
plt.xlabel('Time (s)',fontsize=12,fontstyle = 'italic');
plt.ylabel('Amplitude (a.u.)',fontsize=12,fontstyle = 'italic');
plt.legend(loc = 'upper right',facecolor='white', framealpha=1);
plt.grid()
plt.show()

# Constelação

constI = INTER.getresult("VSA_1","waveform/constellation I")
constI = constI['amplitude (a.u.)']
constQ = INTER.getresult("VSA_1","waveform/constellation Q")
constQ = constQ['amplitude (a.u.)']

plt.figure(3)
plt.plot(constI, constQ,'o', linewidth=2.0)
plt.xlabel('Time (s)',fontsize=12,fontstyle = 'italic');
plt.ylabel('Amplitude (a.u.)',fontsize=12,fontstyle = 'italic');
plt.grid()
plt.show()

# Diagrama de olho
plt.figure(4)
eye = INTER.getresult("EYE_1","eye diagram")
eyeI = eye['amplitude (a.u.)']
time = eye['time']
plt.plot(1e12*time,eyeI,lw = 0.5)
plt.xlabel('time (ps)',fontstyle = 'italic',fontsize = 12)
plt.grid()